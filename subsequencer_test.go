package subsequencer

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLargestSequence(t *testing.T) {
	t.Run("when there is a sequence that is clearly larger than the others", func(t *testing.T) {
		input := []sequence{
			sequence{10, 11, 12, 13, 14},
			sequence{1},
		}

		result := largestSequence(input)

		t.Run("it returns the larger of the two sequences", func(t *testing.T) {
			expected := sequence{10, 11, 12, 13, 14}

			require.Equal(t, expected, result)
		})
	})

	t.Run("when all sequences are the same length", func(t *testing.T) {
		input := []sequence{
			sequence{10, 11},
			sequence{8, 9},
		}

		result := largestSequence(input)

		t.Run("it returns the first sequence", func(t *testing.T) {
			expected := sequence{10, 11}

			require.Equal(t, expected, result)
		})
	})

	t.Run("when provided an empty array", func(t *testing.T) {
		input := make([]sequence, 0)
		result := largestSequence(input)

		t.Run("it returns an empty sequence", func(t *testing.T) {
			expected := make(sequence, 0)

			require.Equal(t, expected, result)
		})
	})
}

func TestSplitSequences(t *testing.T) {
	t.Run("with two full subsequences", func(t *testing.T) {
		input := []int{10, 11, 12, 13, 14, 1, 2, 3, 4}
		result := splitSequences(input)

		t.Run("it returns both sequences in order", func(t *testing.T) {
			expected := []sequence{
				sequence{10, 11, 12, 13, 14},
				sequence{1, 2, 3, 4},
			}

			require.Equal(t, expected, result)
		})
	})

	t.Run("with one full sequence and multiple atomic sequences", func(t *testing.T) {
		input := []int{4, 3, 2, 1, 10, 11, 12, 13, 14, 5}
		result := splitSequences(input)

		t.Run("it returns all of the sequences in order", func(t *testing.T) {
			expected := []sequence{
				sequence{4},
				sequence{3},
				sequence{2},
				sequence{1, 10, 11, 12, 13, 14},
				sequence{5},
			}

			require.Equal(t, expected, result)
		})
	})

	t.Run("with nothing but atomic sequences", func(t *testing.T) {
		input := []int{10, 9, 8, 7}
		result := splitSequences(input)

		t.Run("it returns all of the sequences in order", func(t *testing.T) {
			expected := []sequence{
				sequence{10},
				sequence{9},
				sequence{8},
				sequence{7},
			}

			require.Equal(t, expected, result)
		})
	})

	t.Run("when provided an empty array", func(t *testing.T) {
		input := make([]int, 0)
		result := splitSequences(input)

		t.Run("it returns an empty sequence list", func(t *testing.T) {
			expected := make([]sequence, 0)

			require.Equal(t, expected, result)
		})
	})
}

func TestGetseq(t *testing.T) {
	input := []int{10, 11, 12, 13, 14, 1, 2, 3, 4}

	t.Run("when there are multiple sequences from the given index", func(t *testing.T) {
		index := 0
		result, nextIdx := getseq(input, index)

		t.Run("it provides the first sequence at the given index", func(t *testing.T) {
			expected := sequence{10, 11, 12, 13, 14}

			require.Equal(t, expected, result)
		})

		t.Run("it provides the index for the next sequence", func(t *testing.T) {
			expected := 5

			require.Equal(t, expected, nextIdx)
		})
	})

	t.Run("when there is a single sequence from the given index", func(t *testing.T) {
		index := 5
		result, nextIdx := getseq(input, index)

		t.Run("it provides that sequence", func(t *testing.T) {
			expected := sequence{1, 2, 3, 4}

			require.Equal(t, expected, result)
		})

		t.Run("it returns a negative next index", func(t *testing.T) {
			expected := -1

			require.Equal(t, expected, nextIdx)
		})
	})

	t.Run("when given an empty array", func(t *testing.T) {
		empty := make([]int, 0)
		index := 0

		result, nextIdx := getseq(empty, index)

		t.Run("it returns an empty sequence", func(t *testing.T) {
			expected := sequence{}

			require.Equal(t, expected, result)
		})

		t.Run("it returns a negative next index", func(t *testing.T) {
			expected := -1

			require.Equal(t, expected, nextIdx)
		})
	})

}

func TestSubsequencer(t *testing.T) {
	t.Run("when there is one subsequence", func(t *testing.T) {
		input := []int{25, 2, 3, 4, 1}
		expected := []int{2, 3, 4}
		result := Subsequencer(input)

		t.Run("it returns the subsequence", func(t *testing.T) {
			require.Equal(t, expected, result, fmt.Sprintf("expected %v, got %v", expected, result))
		})
	})

	// [5, 1, 2, 3, 6, 7, 23, 10, 11, 12, 14]
	t.Run("when there are multiple subsequences", func(t *testing.T) {
		input := []int{5, 1, 2, 3, 6, 7, 23, 10, 11, 12, 14}
		expected := []int{1, 2, 3, 6, 7, 23}
		result := Subsequencer(input)

		t.Run("it returns the longest subsequence", func(t *testing.T) {

			require.Equal(t, expected, result, fmt.Sprintf("expected %v, got %v", expected, result))
		})
	})

	t.Run("when there are no multi-item subsequences", func(t *testing.T) {
		input := []int{10, 9, 8, 7, 6}
		expected := []int{10}
		result := Subsequencer(input)

		t.Run("it returns a single-item slice containing the largest candidate", func(t *testing.T) {
			require.Equal(t, expected, result, fmt.Sprintf("expected %v, got %v", expected, result))
		})
	})

	t.Run("when the input is an empty slice", func(t *testing.T) {
		input := make([]int, 0)
		result := Subsequencer(input)

		t.Run("it returns an empty slice", func(t *testing.T) {
			require.Equal(t, len(result), 0, fmt.Sprintf("expected empty array, got %v", result))
		})
	})
}
