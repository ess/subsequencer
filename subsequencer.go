package subsequencer

// Find the Longest Increasing Subsequence: Given an array of integers,
// write a function that finds the longest increasing subsequence.

// [5, 1, 2, 3, 6, 7, 23, 10, 11, 12, 14]

type sequence []int

func newseq() sequence {
	return make(sequence, 0)
}

func getseq(input sequence, start int) (sequence, int) {
	next := -1
	result := newseq()

	// let's shortcut this thing if the input is empty
	if len(input) == 0 {
		return result, next
	}

	result = append(result, input[start])
	current := start + 1

	for current < len(input) {
		next = current

		if input[current] < result[len(result)-1] {
			break
		}

		result = append(result, input[current])
		current += 1
	}

	if current == len(input) {
		next = -1
	}

	return result, next
}

func splitSequences(input []int) []sequence {
	seqs := make([]sequence, 0)
	current := 0

	for current > -1 {
		seq, next := getseq(input, current)

		if len(seq) > 0 {
			seqs = append(seqs, seq)
		}

		if next < 0 {
			break
		}
		current = next
	}

	return seqs
}

func largestSequence(sequences []sequence) sequence {
	output := make(sequence, 0)

	for _, seq := range sequences {
		if len(seq) > len(output) {
			output = seq
		}
	}

	return output
}

func Subsequencer(input []int) []int {
	return largestSequence(splitSequences(input))
}
